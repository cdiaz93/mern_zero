import mongoose from "mongoose";

export const User = () =>{
    let userSchema = new mongoose.Schema({
        name: String,
        email: String,
        age: Number
    });

    return mongoose.model('Users', userSchema);
}