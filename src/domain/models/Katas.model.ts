import mongoose from "mongoose";

export const Katas = () => {
    let katasSchema = new mongoose.Schema({
        Name:           String,
        Description:    String,
        Level:          Number,
        User:           Number,
        Date:           Date,
        Valoration:     Number,
        Chances:        Number
    })

    return mongoose.model('Katas', katasSchema)
}
