import { User } from "../models/User.model";
import { LogSuccess, LogError } from "../../utils/logger";

//CRUD

/**
 * Method to obtain all users from Collection "Users" in Mongo Server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {

    try {
        let userModel = User();


        userModel.find();
        return await userModel.find({isDelete: false});

    } catch(error) {
        LogError(`[ORM ERROR]: getAllUsers: ${error}`);
    }
}

//TODO:
//Ger User by ID
//Get User by Email
//Delete User by ID
//Create New User
//Update User by ID