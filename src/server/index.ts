
import express, { Express, Request, Response } from "express";


//Swahher
import swaggerUi from 'swagger-ui-express';


// * Security
import cors from 'cors';
import helmet from 'helmet';


//TODO HTTPS


// * Root Router
import rootRouter from '../routes'; //Por defecto importa routes/index.ts
import mongoose from "mongoose";

// * Create App Express
const server: Express = express();

// * Swagger Config and route
server.use(
    '/docs',
    swaggerUi.serve, 
    swaggerUi.setup(undefined, {
        swaggerOptions:{
            url: "/swagger.json",
            explorer: true
        }
    }
));

// * Define Server to use "/api" and use rootRouter from 'index.ts' in routes
//From this point onover: http://localhost:8080/api/...
server.use('/api',rootRouter);

//Static Server
server.use(express.static('public'))


//TODO Mongoose Connection
const dbURI='mongodb://127.0.0.1:27017/mern_zero';
mongoose.connect(dbURI);
mongoose.connection.on('connected', () => { console.log('Conexión exitosa a la base de datos'); }); 
mongoose.connection.on('error', (err) => { console.log('Error de conexión a la base de datos:', err); });


// * Security Config
server.use(helmet());
server.use(cors());


// * Content Type Config
server.use(express.urlencoded({extended: true, limit:'50mb'}));
server.use(express.json({limit:'50mb'}));


// * Redirection Config
//http://localhost:8080/  --> http://localhost:8080/api/
server.get('/',(req: Request, res: Response) => {
    res.redirect('/api');
});


export default server;


