import { BasicResponse } from "@/controller/types";
import express, { Request, Response } from "express";
import { HelloController } from "../controller/helloController";
import { LogInfo } from "../utils/logger";

//Router from express
let helloRouter = express.Router();

//http://localhost/api/hello?name=Carlos/
helloRouter.route('/')
    .get(async(req: Request, res: Response) => {
        //Obtain a query param
        let name:any = req?.query?.name;
        LogInfo(`Query Param: ${name}`);

        const controller:HelloController = new HelloController();                   //Controller instance to execute method
        const response:BasicResponse = await controller.getMessage(name);           //Obtain response
        return res.send(response);                                                  //Send to the client the response
    })

//Export helloRouter
export default helloRouter;