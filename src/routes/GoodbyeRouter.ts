import  express,{ Request, Response } from "express";
import { LogInfo } from "../utils/logger";
import { GoodbyeController } from "../controller/goodbyeController";

//Router from express
let goodbyeRouter = express.Router();

goodbyeRouter.route('/')

.get(async(req: Request, res: Response) => {
    //Obtain query params
    let name:any = req?.query?.name;
    LogInfo(`Query Param: ${name}`);

    const controller:GoodbyeController = new GoodbyeController();   //Controller instance to execute method
    const response = await controller.getMessage(name);             //Obtain response
    return res.send(response);                                      //Send to the client the response

})

//Export GodbyeRouter
export default goodbyeRouter;