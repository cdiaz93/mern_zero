/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from "express";
import helloRouter from "./HelloRouter";
import goodbyeRouter from "./GoodbyeRouter"
import { LogInfo } from  "../utils/logger";

//Server instance
let server = express();


//Root Router instance
let rootRouter = express.Router();

//Activate for request to http://localhost:8080/api
rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8080/api');
    res.send('APP Express + TS + Nodemon + Jest + Swagger + Mongoose');
});

//Redirections to Routers & Controllers
server.use('/',rootRouter);                 //http://localhost:8080/api/
server.use('/hello', helloRouter);          //http://localhost:8080/api/hello/    --> HelloRouter
server.use('/goodbye', goodbyeRouter);      //http://localhost:8080/api/goodbye/  --> GoodbyeRouter

//Add more routes to the app

export default server;