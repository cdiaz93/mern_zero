/**
 * Basic JSON response for controllers 
 */
export type BasicResponse = {
    message: string 
}

/**
 * Error JSON response for controllers 
 */
export type ErrorResponse = {
    error: string,
    message: string
}

/**
 * Message with Date JSON response for controllers 
 */
export type MessageDateResponse ={
    date: Date,
    message: string
}