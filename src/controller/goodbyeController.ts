import { MessageDateResponse } from "./types";
import { IGoodbyeController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

export class GoodbyeController implements IGoodbyeController{
    public async getMessage(name?: string | undefined): Promise<MessageDateResponse> {
        LogSuccess('[/api/goodbye] Get Request');

        return{
            date: new Date(),
            message: `Goodbye ${name || "Anonymous"}`
        }
    }

}