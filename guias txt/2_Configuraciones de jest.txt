-----------------------------------------
CONFIGURACION DE JEST (PRUEBAS UNITARIAS)
-----------------------------------------
Una vez ya configurado jest:

1. Crear carpeta en la ruta raiz del proyecto "__test__"
2. dentro de carpeta crear archivos de prueba: las extesniones son:(Ej: prueba1.spec.ts)
3. Poner el codigo de prueba en el archivo.

4.Dentro de package.json en "scripts" agregar nueva linea:
"serve:coverage": "npm run test && cd coverage/lcov-report && npx serve"
(para ejecutar comando por consola> npm run serve:coverage)

5. Para ejecutar pruebas por consola> npm run test

Notas:
- Por lo general el paso 4 se sirve por http://localhost:3000/

(ESTO ES LO BASICO, LUEGO SE IRA ACTUALIZANDO ESTA GUIA CUANDO SE LLEGAN A PRUEBAS)