/////////////////////////////////
Subir Cambios locales a GIT LAB
/////////////////////////////////

Por comandos en la ruta raiz del proyecto ejecutar:

(Este comando se ejecuta unicamente una sola vez, y solo para vincular mi proyecto local con un repositocio en gitlab)
git remote add origin https://gitlab.com/cdiaz93/mern_zero.git

//Configuracines globales de usuario (Solo se ejecuta 1 sola vez)
git config --global user.email "carl894@hotmail.com"
git config --global user.name "cdiaz93"


//Comandos Recurrentes:
ruta>git add .
ruta>git status 
ruta>git commit -m "Descripcion del commit"
ruta>git push

